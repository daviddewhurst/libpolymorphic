/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef FORWARD_H
#define FORWARD_H

#include <record.hpp>

#include <utility>
#include <functional>

template<typename A, typename I, typename O>
std::pair<O, double> forward_step(Record<A>& r, std::function<O(Record<A>&, I)>& f, I input) {
    O output = f(r, input); 
    return std::pair<O, double>(output, r.get_logprob());
}

template<typename A, typename I, typename O>
std::pair<O, double> forward_step(Record<A>& r, O(*f)(Record<A>& r, I input), I input) {
    O output = f(r, input);
    return std::pair<O, double>(output, r.get_logprob());
}

template<typename A, typename I, typename O>
std::pair<std::vector<Record<A>>, std::vector<double>> forward(std::function<O(Record<A>&, I)>& f, I input, unsigned num_iterations) {
    std::pair<O, double> step_result;
    std::vector<Record<A>> records;
    std::vector<double> weights;
    Record<A> r;

    for (unsigned n = 0; n != num_iterations; n++) {
        r = Record<A>();
        step_result = forward_step<A, I, O>(r, f, input);
        records.push_back(r);
        weights.push_back(step_result.second);
    }
    return std::pair<std::vector<Record<A>>, std::vector<double>>(std::move(records), std::move(weights));
}

template<typename A, typename I, typename O>
std::pair<std::vector<Record<A>>, std::vector<double>> forward(O(*f)(Record<A>& r, I input), I input, unsigned num_iterations) {
    std::pair<O, double> step_result;
    std::vector<Record<A>> records;
    std::vector<double> weights;
    Record<A> r;

    for (unsigned n = 0; n != num_iterations; n++) {
        r = Record<A>();
        step_result = forward_step<A, I, O>(r, f, input);
        records.push_back(r);
        weights.push_back(step_result.second);
    }
    return std::pair<std::vector<Record<A>>, std::vector<double>>(std::move(records), std::move(weights));
}

#endif // FORWARD_H