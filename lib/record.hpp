/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef RECORD_H
#define RECORD_H

#include <cstddef>
#include <string>
#include <map>
#include <any>
#include <utility>
#include <functional>

#include <node.hpp>

/**
 * A record stores values and their corresponding Nodes at unique addresses. 
 * 
 * @tparam A the address type. Reasonable choices could be std::string or unsigned.
 */ 
template<typename A>
class Record {
    private:
        std::map<A, std::any> _address_map;
        std::map<A, std::any> _value_map;
        double _logprob;
        double _loglikelihood;

    public:
        /**
         * Constructs an empty record.
         */ 
        Record() {
            _address_map = std::map<A, std::any>();
            _value_map = std::map<A, std::any>();
            _logprob = 0.0;
            _loglikelihood = 0.0;
        }

        /**
         * Destroys the contents of the record and sets logprob and loglikelihood to zero.
         */ 
        void clear() {
            _logprob = 0.0;
            _loglikelihood = 0.0;
            _address_map.clear();
            _value_map.clear();
        }

        bool is_empty() {
            return (_address_map.size() == 0) & (_value_map.size() == 0);
        }

        bool contains(A address) {
            return _address_map.contains(address);
        } 

        /**
         * Gets a pointer to the address's node.
         * 
         * @tparam D the type of the distribution at that address
         * @param address the address to query
         * @return a pointer to a Node<D>
         */ 
        template<typename D>
        Node<D> * get_node_ref(A address) {
            return std::any_cast<Node<D>>(&_address_map[address]);
        }

        /**
         * Sets the node at the address. Does *not* set a corresponding value which must be done separately.
         * Not recommended for external use.
         * 
         * @tparam D the type of the distribution at that address
         * @param address the address for which to set the node
         * @param node the node to set
         * @param sametype_safe if true, assumes that the type of the node to set is the same type that was in the
         *  record previously and automatically updates record log probability and log likelihood. If sametype_safe
         *  is false, this method does *not* automatically update record log probability or log likelihood which could
         *  lead to arbitrarily poor results during inference.
         */ 
        template<typename D>
        void set_node(A address, Node<D> node, bool sametype_safe = true) {
            if (sametype_safe) { 
                if (_address_map.contains(address)) {
                    remove_logprob<D>(address);
                }
            }
            _address_map[address] = node;
            double lp = node.get_logprob();
            _logprob += lp;
            if (node.is_observed()) {
                _loglikelihood += lp;
            }
        }

        /**
         * Gets a pointer to the address's value.
         * 
         * @tparam V the value type
         * @param address the address to query
         * @return a pointer to the value
         */ 
        template<typename V>
        V * get_value_ref(A address) {
            return std::any_cast<V>(&_value_map[address]);
        }

        /**
         * Sets a value at the address.
         * 
         * @tparam V the value type
         * @param address the address for which to set the value 
         * @param value the value to set
         * 
         */ 
        template<typename V>
        void set_value(A address, V value) {
            _value_map[address] = value;
        }

        /**
         * Removes the address's node's log probability from the record. Effectively converts traced randomness
         * to untraced randomness. 
         * 
         * @tparam D the type of the distribution at the address
         * @param address the address for which to remove the log probability
         */ 
        template<typename D>
        void remove_logprob(A address) {
            Node<D> * node_ref = get_node_ref<D>(address);
            double lp = node_ref->get_logprob();
            _logprob -= lp;
            if (node_ref->is_observed()) {
                _loglikelihood -= lp;
            }
        }

        void add_logprob(double lp) { _logprob += lp; }

        double get_logprob() {
            return _logprob;
        }
        double get_loglikelihood() {
            return _loglikelihood;
        }

        template<typename V, typename D>
        std::string summarize(A address) {
            return "value = " + std::to_string(*(this->template get_value_ref<V>(address))) + ", node = " + this->template get_node_ref<D>(address)->string();
        }
};  // class Record

template<typename A, typename RNG, typename V, typename D>
std::pair<V, double> sample(Record<A>& record, A address, RNG& rng, D dist) {
    if (record.contains(address)) {
        auto i = record.template get_node_ref<D>(address)->get_interpretation();
        if (i == STANDARD) {
            return _sample_standard<A, RNG, V, D>(record, address, rng, dist);
        }
        else if (i == REPLAY) {
            return _sample_replay<A, V, D>(record, address, dist);
        }
        else if (i == CONDITION) {
            return _sample_condition<A, V, D>(record, address, dist);
        }
        else if (i == REWRITE) {
            return _sample_passthrough<A, V>(record, address);
        }
    } else {
        return _sample_standard<A, RNG, V, D>(record, address, rng, dist);
    }
}

template<typename A, typename RNG, typename V, typename D>
std::pair<V, double> _sample_standard(Record<A>& record, A address, RNG& rng, D dist) {
    std::pair<V,Node<D>> value_and_node = sample<RNG, V, D>(rng, dist);
    auto retval = std::pair<V, double>(value_and_node.first, value_and_node.second.get_logprob());
    record.set_node(address, value_and_node.second);
    record.set_value(address, value_and_node.first);
    return retval;
}

template<typename A, typename V, typename D>
std::pair<V, double> _sample_replay(Record<A>& record, A address, D dist) {
    double the_value = *record.template get_value_ref<V>(address);
    double new_score = record.template get_node_ref<D>(address)->get_dist().logprob(the_value);
    // replace the score in the node, and control for this in the global logprob counter
    record.template remove_logprob<D>(address);  // remove old lp
    record.template get_node_ref<D>(address)->set_logprob(new_score);  // reset lp in the newly-scored (value, node) pair
    record.add_logprob(new_score);  // replace lp in the global counter
    return std::pair<V, double>(the_value, new_score);
}

template<typename A, typename V, typename D>
std::pair<V, double> _sample_condition(Record<A>& record, A address, D dist) {
    double the_value = *record.template get_value_ref<V>(address);
    double new_score = record.template get_node_ref<D>(address)->get_dist().logprob(the_value);
    // replace the score in the node, and control for this in the global logprob counter
    record.template remove_logprob<D>(address);  // remove old lp
    record.template get_node_ref<D>(address)->set_logprob(new_score);  // reset lp in the newly-scored (value, node) pair
    record.add_logprob(new_score);  // replace lp in the global counter
    return std::pair<V, double>(the_value, new_score);
}

template<typename A, typename V>
std::pair<V, double> _sample_passthrough(Record<A>& record, A address) {
    return std::pair<V, double>(*record.template get_value_ref<V>(address), record.get_logprob());
}

template<typename A, typename V, typename D>
std::pair<V, double> observe(Record<A>& record, A address, V obs, D dist) {
    std::pair<V,Node<D>> value_and_node = observe<V, D>(obs, dist);
    auto retval = std::pair<V, double>(value_and_node.first, value_and_node.second.get_logprob());
    record.set_node(address, value_and_node.second);
    record.set_value(address, value_and_node.first);
    return retval;
}

template<typename A, typename I, typename O>
O track(Record<A>& record, A address, std::function<O(I)>& f, I input) {
    O value = f(input);
    auto node = Node<BlackBox<I, O>>(BlackBox<I, O>(input, value), 0.0, false);
    record.set_node(address, std::move(node));
    record.set_value(value);
    return value;
}

template<typename A, typename I, typename O>
O track(Record<A>& record, A address, O(*f)(I input), I input) {
    O value = f(input);
    auto node = Node<BlackBox<I, O>>(BlackBox<I, O>(input, value), 0.0, false);
    record.set_node(address, std::move(node));
    record.set_value(value);
    return value;
}

#endif  // RECORD_H