/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef QUERY_H
#define QUERY_H

#include <cmath>
#include <limits>

#include <utility.hpp>
#include <query/query_streams.hpp>

namespace Query {

    template<typename V>
    V weighted_mean(std::vector<std::pair<V, double>>& query_result) {
        V retval;
        auto the_size = query_result.size();
        for (unsigned ix = 0; ix != the_size; ix++) {
            retval += query_result[ix].first * query_result[ix].second;
        }
        return retval;
    }

    template<typename V>
    V weighted_stddev(std::vector<std::pair<V, double>>& query_result) {
        V retval, _mean;
        auto the_size = query_result.size();
        for (unsigned ix = 0; ix != the_size; ix++) {
            _mean += query_result[ix].first * query_result[ix].second;
        }
        for (unsigned ix = 0; ix != the_size; ix++) {
            retval += std::pow(query_result[ix].first - _mean, 2.0) * query_result[ix].second;
        }
        return std::sqrt(retval);
    }

    template<typename V>
    V mode(std::vector<std::pair<V, double>>& query_result) {
        V retval;
        double max_lp = -std::numeric_limits<double>::infinity();
        auto the_size = query_result.size();
        for (unsigned ix = 0; ix != the_size; ix++) {
            if (query_result[ix].second > max_lp) {
                retval = query_result[ix].first;
                max_lp = query_result[ix].second;
            }
        }
        return retval;
    }

    template<typename A>
    class WeightedSamplePosterior {
        private:
            std::pair<std::vector<Record<A>>, std::vector<double>>& _results_ref; 
            bool _normalized;
        public:
            WeightedSamplePosterior(
                std::pair<std::vector<Record<A>>, std::vector<double>>& results_ref,
                bool normalize = false
            ) : _results_ref(results_ref) {
                if (normalize) {
                    normalize_weights_ref(results_ref.second);
                }
                _normalized = normalize;
            }

            template<typename V>
            std::vector<std::pair<V, double>> query(A address) {
                auto retval = std::vector<std::pair<V, double>>();
                for (unsigned ix = 0; ix != _results_ref.first.size(); ix++) {
                    if (_results_ref.first[ix].contains(address)) {
                        retval.push_back(
                            std::pair<V, double>(
                                *_results_ref.first[ix].template get_value_ref<V>(address),
                                _results_ref.second[ix]
                            )
                        );
                    }
                }
                return retval;
            }

            template<typename V>
            V reduce(A address, V(*reducer)(std::vector<std::pair<V, double>>& r)) {
                std::vector<std::pair<V, double>> stream = query<V>(address);
                return reducer(stream);
            }
    
    };

};  // namespace Query

#endif  // QUERY_H
