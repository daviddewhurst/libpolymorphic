/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef NODE_H
#define NODE_H

#include <string>
#include <vector>
#include <random>
#include <utility>

#include <distributions.hpp>

using namespace Distributions;

/**
 * Defines semantics of how a node should be interpreted.
 * 
 * Non-empty records (see record.hpp) may be passed to probabilistic programs,
 * or to inference algorithms. An address's node/value in a record may be 
 * interpreted in a variety of ways.
 * 
 * STANDARD: pretend that the address does not have a corresponding node/value and
 *  perform originally intended actions
 * REPLAY: do not generate a value, but instead score the existing value against the 
 *  distribution in the node, replacing the logprob of the node with the new log 
 *  probability. Do not change the value of observe. Undefined behavior if there 
 *  is no corresponding value to a node.
 * CONDITION: currently identical to REPLAY.
 * REDISTRIBUTE: given a value and a new distribution, score the value as though it were
 *  sampled from the new distribution and replace the existing node with one containing the
 *  new score and new distribution.
 */ 
enum Interpretation {STANDARD, REPLAY, CONDITION, REWRITE};

/**
 * A Node represents an equivalence class of random variates.
 * Node(dist, lp, obs) represents {x: x \in dom(dist), p(x) = lp, isObserved(x) = obs}
 * 
 * @tparam D the distribution type
 */ 
template<typename D>
class Node {
    public:
        /**
         * Represents an equivalence class of random variates
         * 
         * @param dist the associated probability distribution
         * @param lp a log probability, could correspond to log probability of some value under the distribution
         * @param obs whether or not a value was observed
         */ 
        Node(D dist, double logprob, bool observed, Interpretation interpretation = STANDARD) {
            _dist = dist;
            _logprob = logprob;
            _observed = observed;
            _interpretation = interpretation;
        }

        Interpretation get_interpretation() { return _interpretation; }
        void set_interpretation(Interpretation interpretation) { _interpretation = interpretation; }
        
        /**
         * Returns a copy of the log probability.
         */ 
        double get_logprob() {
            return _logprob;
        }

        /**
         * Sets the log probability. External usage of this method is not recommended.
         */ 
        void set_logprob(double logprob) {
            _logprob = logprob;
        }

        /**
         * Returns a copy of the distribution.
         */ 
        D get_dist() {
            return _dist;
        }

        /**
         * Sets the distribution. External usage of this method is not recommended.
         */ 
        void set_dist(D dist) {
            _dist = dist;
        }

        /**
         * Returns a copy of the observed status.
         */ 
        bool is_observed() {
            return _observed;
        }

        /**
         * Sets the observed status. External usage of this method is not recommended.
         */ 
        void set_observed(bool status) {
            _observed = status;
        }

        /**
         * Returns a std::string representation of the node: "Node(dist=dist, logprob=logprob, obs=obs)".
         */ 
        std::string string() const {
            std::string is_obs = _observed ? "true" : "false";
            return "Node(dist=" + _dist.string() + ", logprob=" + std::to_string(_logprob) + ", obs=" + is_obs + ")";
        }

    private:
        D _dist;
        double _logprob;
        bool _observed;
        Interpretation _interpretation;


};  // class Node

/**
 * Samples a random variate according to the distribution dist using the next random number from rng.
 * 
 * @tparam RNG the type of the random number generator. Must be able to be used by dist.sample.
 * @tparam V the type of the value to be sampled from dist.
 * @tparam D the type of dist.
 * @param rng the random number generator. Only tested with PRNGs in std.
 * @param dist the distribution according to which the value will be drawn.
 * @return (sampled value, node) pair
 */ 
template<typename RNG, typename V, typename D>
std::pair<V, Node<D>> sample(RNG& rng, D dist) {
    V value = dist.sample(rng);
    double lp = dist.logprob(value);
    Node<D> n = Node(dist, lp, false);
    return std::pair<V, Node<D>>(value, n);
}

/**
 * Scores an outside value according to the distribution dist.
 * 
 * @tparam V the type of the value to be scored against dist.
 * @tparam D the type of dist.
 * @param value the value to be scored. 
 * @param dist the distribution according to which the value will be drawn.
 * @return (scored value, node) pair
 */ 
template<typename V, typename D>
std::pair<V, Node<D>> observe(V value, D dist) {
    double lp = dist.logprob(value);
    Node<D> n = Node(dist, lp, true);
    return std::pair<V, Node<D>>(value, n);
}

#endif  // NODE_H