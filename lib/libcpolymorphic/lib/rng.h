/* 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef RNG_H
#define RNG_H

#define __POW2_M64   5.421010862427522170037264004349e-020

#include <stdint.h>

struct xorshift64s_state { uint64_t x; };
typedef struct xorshift64s_state XOR64State;

uint64_t xorshift64s (XOR64State * state);
double randu_xorshift64s(XOR64State * state);

#endif  // RNG_H
