/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef QUERY_STREAMS_H
#define QUERY_STREAMS_H

#include <limits>

template<typename V, typename A>
class StreamWeightedMean {
    private:
        double _weight_sum;
        V _mean;
    public:
        StreamWeightedMean() {
            _weight_sum = 0.0;
            _mean = 0.0;
        }
        void update(Record<A>& r, A& address, double weight) {
            _weight_sum += weight;
            V value = *r.template get_value_ref<V>(address);
            _mean += (weight / _weight_sum) * (value - _mean);
        }
        V emit() {
            return _mean;
        }
};


template<typename V, typename A>
class StreamMode {
    private:
        double _lp;
        V _mode;
    public:
        StreamMode() {
            _lp = -std::numeric_limits<double>::infinity();
        }
        void update(Record<A>& r, A& address, double weight) {
            if (weight > _lp) {
                _mode = *r.template get_value_ref<V>(address);
                _lp = weight;
            }
        }
        V emit() {
            return _mode;
        }
};

#endif  // QUERY_STREAMS_H