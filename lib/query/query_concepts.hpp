/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef QUERY_CONCEPTS_H
#define QUERY_CONCEPTS_H

#if __has_include(<concepts>)
#include <concepts>

template<typename T>
concept Queryable = requires(T t) {
    t.query;
};

template<typename T>
concept Streamer = requires(T t) {
    t.update;
    t.emit;
};

#endif  // __has_include(<concepts>)

#endif  // QUERY_CONCEPTS_H
