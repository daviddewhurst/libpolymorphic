/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef INTERPRETATIONS_H
#define INTERPRETATIONS_H

#include <node.hpp>
#include <record.hpp>

template<typename A, typename V, typename D>
void replay(Record<A>& r, A address, V value) {
    r.template get_node_ref<D>(address)->set_interpretation(REPLAY);
    r.template set_value<V>(address, value);
}

template<typename A, typename D>
void standard(Record<A>& r, A address) {
    r.template get_node_ref<D>(address)->set_interpretation(STANDARD);
}

template<typename A, typename V, typename D>
void condition(Record<A>& r, A address, V value) {
    r.template get_node_ref<D>(address)->set_interpretation(CONDITION);
    r.template set_value<V>(address, value);
}

template<typename A, typename V, typename D1, typename D2>
void rewrite(Record<A>& r, A address, V value, D2 new_dist) {
    Node<D2> new_node = Node<D2>(
        new_dist,
        new_dist.logprob(value),
        r.template get_node_ref<D1>(address)->is_observed(),
        REWRITE
    );
    r.template remove_logprob<D1>(address);
    r.template set_node<D2>(address, new_node, false);
    r.template set_value<V>(address, value);
}

template<typename A, typename V, typename D1, typename D2>
void redistribute(Record<A>& r, A address, D2 new_dist) {
    rewrite<A, V, D1, D2>(r, address, *r.template get_value_ref<V>(address), new_dist);
}

#endif  // INTERPRETATIONS_H