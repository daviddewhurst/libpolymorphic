/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef IMPORTANCE_H
#define IMPORTANCE_H

#include <node.hpp>
#include <record.hpp>

#if __has_include(<concepts>)
#include <query/query_concepts.hpp>
#endif

#include <cstddef>
#include <utility>
#include <functional>

template<typename A, typename I, typename O>
std::pair<O, double> likelihood_weighting_step(Record<A>& r, std::function<O(Record<A>&, I)>& f, I input) {
    O output = f(r, input); 
    return std::pair<O, double>(output, r.get_loglikelihood());
}

template<typename A, typename I, typename O>
std::pair<O, double> likelihood_weighting_step(Record<A>& r, O(*f)(Record<A>& r, I input), I input) {
    O output = f(r, input);
    return std::pair<O, double>(output, r.get_loglikelihood());
}

template<typename A, typename I, typename O>
std::pair<std::vector<Record<A>>, std::vector<double>> likelihood_weighting(std::function<O(Record<A>&, I)>& f, I input, unsigned num_iterations) {
    std::pair<O, double> step_result;
    std::vector<Record<A>> records;
    std::vector<double> weights;
    Record<A> r;

    for (unsigned n = 0; n != num_iterations; n++) {
        r = Record<A>();
        step_result = likelihood_weighting_step<A, I, O>(r, f, input);
        records.push_back(r);
        weights.push_back(step_result.second);
    }
    return std::pair<std::vector<Record<A>>, std::vector<double>>(std::move(records), std::move(weights));
}

template<typename A, typename I, typename O>
std::pair<std::vector<Record<A>>, std::vector<double>> likelihood_weighting(O(*f)(Record<A>& r, I input), I input, unsigned num_iterations) {
    std::pair<O, double> step_result;
    std::vector<Record<A>> records;
    std::vector<double> weights;
    Record<A> r;

    for (unsigned n = 0; n != num_iterations; n++) {
        r = Record<A>();
        step_result = likelihood_weighting_step<A, I, O>(r, f, input);
        records.push_back(r);
        weights.push_back(step_result.second);
    }
    return std::pair<std::vector<Record<A>>, std::vector<double>>(std::move(records), std::move(weights));
}

template<typename A, typename I, typename O, typename V,
    template<class, class> class Q>
V likelihood_weighting(
    O(*f)(Record<A>& r, I input),
    I input,
    Q<V, A>& queryer,
    A address,  // this is suboptimal -- we should be able to stream-query multiple addresses
    unsigned num_iterations
) {
    std::pair<O, double> step_result;
    Record<A> r = Record<A>();

    for (unsigned n = 0; n != num_iterations; n++) {
        step_result = likelihood_weighting_step<A, I, O>(r, f, input);
        queryer.update(r, address, step_result.second);
        r.clear();
    }
    return queryer.emit();
}

#endif  // IMPORTANCE_H