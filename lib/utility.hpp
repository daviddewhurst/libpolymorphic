#ifndef UTILITY_H
#define UTILITY_H

#include <vector>

double logsumexp(const std::vector<double>& values);

double sumexp(const std::vector<double>& values, bool stable=true);

bool double_allclose(double x, double y);

void normalize_weights_ref(std::vector<double>& weights);

std::vector<double> normalize_weights(std::vector<double> weights);

void normalize_values_ref(std::vector<double>& values);

#endif // UTILITY_H