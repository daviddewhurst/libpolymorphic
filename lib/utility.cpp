#include <vector>
#include <cmath>
#include <algorithm>
#include <limits>

double logsumexp(const std::vector<double>& values) {
    double r = 0.0;
    auto the_size = values.size();
    if (the_size > 0) {
        double max_e = *std::max_element(values.begin(), values.end());
        for (unsigned i = 0; i != the_size; i++) {
            r += std::exp(values[i] - max_e);
        }
        return std::log(r) + max_e;
    } else {
        return r;
    }
}

double sumexp(const std::vector<double>& values, bool stable=true) {
    double r = 0.0;
    auto the_size = values.size();
    if (the_size > 0) {
        if (stable) {
            std::vector<double> values_copy = values;
            std::sort(values_copy.begin(), values_copy.end());
            for (unsigned i = 0; i != the_size; i++) {
                r += std::exp(values_copy[i]);
            }
            return r;
        } else {
            for (unsigned i = 0; i != the_size; i++) {
                r += std::exp(values[i]);
            }
            return r;
        }
    } else {
        return 1.0;
    }
}

void normalize_weights_ref(std::vector<double>& weights) {
    auto the_size = weights.size();
    if (the_size > 0) {
        double log_normalizer = logsumexp(weights);
        for (unsigned i = 0; i != the_size; i++) {
            // e^w / sum(e^w) -> w - log(sum(e^w)) = w - logsumexp(w)
            weights[i] = std::exp(weights[i] - log_normalizer);  
        }
    } 
}

void normalize_values_ref(std::vector<double>& values) {
    auto the_size = values.size();
    if (the_size > 0) {
        double the_sum = 0.0;
        for (unsigned i = 0; i != the_size; i++) {
            the_sum += values[i];
        }
        for (unsigned i = 0; i != the_size; i++) {
            values[i] = values[i] / the_sum;
        }
    }
}

std::vector<double> normalize_weights(std::vector<double> weights) {
    auto copy_weights = weights;
    normalize_weights_ref(copy_weights);
    return copy_weights;
}

bool double_allclose(double x, double y) {
    // good enough for our current purposes
    return std::fabs(x - y) <= std::numeric_limits<double>::epsilon();
}