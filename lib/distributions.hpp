/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef DISTRIBUTIONS_H
#define DISTRIBUTIONS_H

#include <iostream>
#include <string>
#include <vector>
#include <random>


namespace Distributions {

    /**
     * Base class from which all other distribution-like objects should inherit.
     */ 
    class Distribution {};

    /**
     * A normal distribution parameterized by location and scale.
     * 
     * @param loc: double corresponding to location (mean)
     * @param scale: non-negative double corresponding to scale (standard deviation)
     */
    class Normal : public Distribution {

        public:
            /**
             * A normal distribution parameterized by location and scale.
             * 
             * @param loc: double corresponding to location (mean)
             * @param scale: non-negative double corresponding to scale (standard deviation)
             */
            Normal(double loc, double scale);

            /**
             * A normal disribution with unit variance.
             */ 
            Normal(double loc);

            /**
             * A normal distribution with zero mean, unit variance.
             */ 
            Normal();

            /**
             * Gets the location parameter.
             * 
             * @return double location parameter
             */
            double get_loc();

            /**
             * Gets the scale parameter.
             * 
             * @return double scale parameter.
             */ 
            double get_scale();

            /**
             * Returns a string representation of the distribution.
             * 
             * @return `std::string` representing the distribution.
             */ 
            std::string string() const;

            /**
             * Samples a random variate from the normal distribution.
             * 
             * @tparam RNG the type of the random number generator used; e.g., std::minstd_rand
             * @param generator the (P)RNG used to generate data
             * @return double number distributed as N(loc, scale)
             */ 
            template<typename RNG>
            double sample(RNG& generator) {
                return _dist(generator);
            }

            /**
             * The log probability of the value under the distribution. 
             * 
             * @param value the value to score
             * @return double the log probability of the value under N(loc, scale)
             */ 
            double logprob(double value) const;

        private:
            double _loc;
            double _scale;
            double _log_Z;
            std::normal_distribution<> _dist;
    };  // class Normal


    /**
     * A categorical distribution parameterized by a std::vector<double> of probabilities
     */
    class Categorical : public Distribution {
        private:
            std::vector<double> _probs;
            std::discrete_distribution<int> _dist;

        public:
            /**
             * A categorical distribution parameterized by a std::vector<double> of probabilities
             * 
             * @param std::vector<double> probs the probability vector of the categorical
             */
            Categorical(std::vector<double> probs);

            /**
             * A categorical distribution with uniform probability of selecting any of 1,...,dim
             * @param dim integer dimension, must be > 1
             */
            Categorical(int dim);

            /**
             * A bivariate categorical distribution with equal probabilities.
             */
            Categorical();

            /**
             * Get the probability vector of the categorical distribution
             */
            std::vector<double> get_probs();

            /**
             * Get the dimension of the categorical distribution
             */
            unsigned long get_dim();

            /**
             * Get a std::string representation of the categorical distribution.
             */
            std::string string() const;

            /**
             * Samples a random variate from the categorical distribution
             * @tparam RNG the type of the random number generator used; e.g., std::minstd_rand
             * @param generator the (P)RNG used to generate data
             * @return unsigned long distributed as Categorical(probs)
             */
            template<typename RNG>
            int sample(RNG& generator) {
                return _dist(generator);
            }

            /**
             * The log probability of the value under the distribution
             * 
             * @param value the value to score under the distribution
             * @return the log probability of the value scored under the distribution
             */
            double logprob(unsigned long value) const;
    };  // class Categorical

    /**
     * A gamma distribution parameterized by shape k and scale theta.
     */
    class Gamma : public Distribution {
        private:
            double _k;
            double _theta;
            double _log_Z;
            std::gamma_distribution<double> _dist;

        public:
            /**
             * A gamma distribution parameterized by shape k and scale theta. 
             * 
             * @param k shape of the distribution
             * @param theta scale of the distribution; rate = 1/scale.
             */
            Gamma(double k, double theta);

            /**
             * A gamma distribution with k = 1; corresponds to exponential distribution with scale = theta
             *
             * @param theta scale of the distribution; rate = 1/scale.
             */
            Gamma(double theta);

            /**
             * A gamma distribution with k = theta = 1; corresponds to a standard exponential distribution.
             */
            Gamma();

            /**
             * Gets the value of the shape parameter
             */
            double get_k();

            /**
             * Gets the value of the scale parameter.
             */
            double get_theta();

            /**
             * Get a std::string representation of the categorical distribution.
             */
            std::string string() const;

            /**
             * Samples a random variate from the gamma distribution
             * @tparam RNG the type of the random number generator used; e.g., std::minstd_rand
             * @param generator the (P)RNG used to generate data
             * @return unsigned long distributed as Gamma(k, theta)
             */
            template<typename RNG>
            double sample(RNG& generator) {
                return _dist(generator);
            }

            /**
             * The log probability of the value under the distribution
             * 
             * @param value the value to score under the distribution
             * @return the log probability of the value scored under the distribution
             */
            double logprob(double value) const;
    };  // class Gamma

    class Poisson : public Distribution {
        private:
            double _rate;
            std::poisson_distribution<unsigned> _dist;

        public:
            Poisson(double rate);
            Poisson();

            double get_rate();

            std::string string() const;

            template<typename RNG>
            unsigned sample(RNG& generator) {
                return _dist(generator);
            }

            double logprob(unsigned value);

    };  // class Poisson

    /**
     * A black box function application with no traced randomness. The function should take exactly
     * one argument.
     *
     * @tparam I the function input type
     * @tparam O the function output type
     */
    template<typename I, typename O>
    class BlackBox : public Distribution {
        private:
            I _input;
            O _output;
        public:
            /**
             * A black box function application with no traced randomness.
             * @param input the input to the function
             * @param output the output of the function
             */
            BlackBox(I input, O output) {
                _input = input;
                _output = output;
            }

            /**
             * Gets the input to the black-box function
             */
            I get_input() { return _input; }

            /**
             * Gets the output of the black-box function
             */
            O get_output() { return _output; }
    };

}  // namespace Distributions

#endif  // DISTRIBUTIONS_H