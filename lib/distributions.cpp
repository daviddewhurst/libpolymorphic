/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#define _USE_MATH_DEFINES

#include "distributions.hpp"

#include <cstddef>
#include <string>
#include <random>
#include <cmath>
// #include <numbers>
#include <sstream>
#include <vector>

using namespace Distributions;

// Normal

Normal::Normal(double loc, double scale) {
    _loc = loc;
    _scale = scale;
    _dist = std::normal_distribution(_loc, _scale);
    //_log_Z = 0.5 * std::log(2.0 * std::numbers::pi_v<double>) + std::log(_scale);
    _log_Z = 0.5 * std::log(2.0 * M_PI) + std::log(_scale);
}
Normal::Normal(double loc) : Normal(loc, 1.0) {}
Normal::Normal() : Normal(0.0, 1.0) {}

double Normal::get_loc() {
    return _loc;
}

double Normal::get_scale() {
    return _scale;
}

std::string Normal::string() const {
    std::string str_loc = std::to_string(_loc);
    std::string str_scale = std::to_string(_scale);
    return "Normal(loc=" + str_loc + ", scale=" + str_scale + ")";
}

double Normal::logprob(double value) const {
    double std_score = (value - _loc) / _scale;
    return -0.5 * std::pow(std_score, 2.0) - _log_Z;
}


// Categorical 

std::vector<double> _to_uniform_vector(int dim) {
    std::vector<double> probs;
    for (int i = 0; i != dim; i++) {
        probs.push_back(1.0 / dim);
    }
    return probs;
}

Categorical::Categorical(std::vector<double> probs) {
    _probs = probs;
    _dist = std::discrete_distribution(_probs.begin(), _probs.end());
}
Categorical::Categorical(int dim) : Categorical(_to_uniform_vector(dim)) {}
Categorical::Categorical() : Categorical(2) {}

std::vector<double> Categorical::get_probs() {
    return _probs;
}

unsigned long Categorical::get_dim() {
    return _probs.size();
}

std::string Categorical::string() const {
    std::stringstream ss;
    ss << "[";
    for ( size_t i = 0; i != _probs.size(); i++) {
        if (i != 0) {
            ss << ",";
        }
        ss << _probs[i];
    }
    ss << "]";
    std::string str_probs = ss.str();
    return "Categorical(probs=" + str_probs + ")";
}

double Categorical::logprob(unsigned long value) const {
    return std::log(_probs[value]);
}

Gamma::Gamma(double k, double theta) {
    _k = k;
    _theta = theta;
    _dist = std::gamma_distribution(_k, _theta);
    _log_Z = _k * std::log(_theta) + lgamma(_k);
}
Gamma::Gamma(double theta) : Gamma::Gamma(1.0, theta) {}
Gamma::Gamma() : Gamma::Gamma(1.0, 1.0) {}

double Gamma::get_k() { return _k; }
double Gamma::get_theta() { return _theta; }

std::string Gamma::string() const {
    return "Gamma(k=" + std::to_string(_k) + ", theta=" + std::to_string(_theta) + ")";
}

double Gamma::logprob(double val) const {
    return (_k - 1.0) * std::log(val) - val / _theta - _log_Z;
}

Poisson::Poisson(double rate) {
    if (rate > 0.0) {
        _rate = rate;
    } else {
        _rate = 0.0;
    }
}
Poisson::Poisson() {
    _rate = 1.0;
}

double Poisson::get_rate() { return _rate; }

std::string Poisson::string() const {
    return "Poisson(rate=" + std::to_string(_rate) + ")";
}

double Poisson::logprob(unsigned value) {
    return -_rate + value * std::log(_rate) - lgamma(value + 1.0);
}
