/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#define _USE_MATH_DEFINES

#include <distributions.hpp>
#include <node.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <limits>

using namespace Distributions;

bool double_allclose(double x, double y) {
    // good enough for our current purposes
    return std::fabs(x - y) <= std::numeric_limits<double>::epsilon();
}

int test_normal_node() {
    std::cout << "~~~ Testing normal node ~~~ \n";
    std::minstd_rand rng(2022);

    auto norm = Normal(0.0, 4.0);
    double val = norm.sample(rng);
    double lp = norm.logprob(val);

    auto normal_node = Node(norm, lp, true);
    std::cout << "Created normal node: " << normal_node.string() << "\n";
    auto latent_normal_node = Node(norm, lp, false);
    std::cout << "Created normal node: " << latent_normal_node.string() << "\n";

    // create normal node from sample interface
    auto value_and_node = sample<std::minstd_rand,double,Normal>(rng, norm);
    std::cout << "Created normal node via sample: " << value_and_node.second.string() << "\n";

    value_and_node = observe<double,Normal>(0.0, norm);
    double true_lp = -0.5 * std::log(2.0 * M_PI) - std::log(4.0);
    if (!double_allclose(true_lp, value_and_node.second.get_logprob())) {
        std::cout << "True log p(0.0|0.0, 4.0) = " << true_lp << " but got " << value_and_node.second.get_logprob() << "\n";
        return 1;
    }
    std::cout << "Created normal node via observe: " << value_and_node.second.string() << "\n";

    return 0;
}


int test_cat_node() {
    std::cout << "~~~ Testing categorical node ~~~ \n";

    std::minstd_rand rng(2022);

    auto cat = Categorical(
        std::vector<double>({0.4, 0.1, 0.2, 0.3})
    );
    double val = cat.sample(rng);
    double lp = cat.logprob(val);

    auto cat_node = Node<Categorical>(cat, lp, true);
    auto value_and_node = sample<std::minstd_rand,unsigned,Categorical>(rng, cat);
    std::cout << "Created cat node: " << cat_node.string() << "\n";

    for (int i = 0; i != 5; i++) {
        value_and_node = sample<std::minstd_rand,unsigned,Categorical>(rng, cat);
        std::cout << "Created cat node from sample: " << cat_node.string() << "\n";
    }
    value_and_node = observe<unsigned,Categorical>(1, cat);
    double true_lp = std::log(0.1);
    if (!double_allclose(true_lp, cat.logprob(1))) {
        std::cout << "True log p(1 | {0.4, 0.1, 0.2, 0.3}) = " << true_lp << " but got " << cat.logprob(1) << "\n";
        return 1;
    }
    std::cout << "Created cat node from observe: " << cat_node.string() << "\n";

    return 0;
}


int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_normal_node(),
        test_cat_node()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}