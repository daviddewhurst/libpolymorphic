/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <distributions.hpp>
#include <node.hpp>
#include <record.hpp>
#include <query.hpp>
#include <importance.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <utility>

using namespace Distributions;
using namespace Query;

std::minstd_rand rng(2022);

double normal_model(Record<std::string>& r, double data) {
    auto loc = sample<std::string, std::minstd_rand, double, Normal>(r, "loc", rng, Normal(0.0, 4.0)); 
    auto log_scale = sample<std::string, std::minstd_rand, double, Normal>(r, "log_scale", rng, Normal(0.0, 3.0));
    auto data_ = observe<std::string, double, Normal>(r, "data", data, Normal(loc.first, std::exp(log_scale.first)));
    return std::pow(data_.first, 2.0);
}

int test_lw_step() {
    std::cout << "Using std::function interface.\n";
    std::function<double(Record<std::string>&,double)> f = normal_model;
    auto r = Record<std::string>();
    double data = 3.0;
    std::pair<double, double> result = likelihood_weighting_step(r, f, data);
    
    std::cout << "Loc node: " << r.get_node_ref<Normal>("loc")->string() << "\n";
    std::cout << "Log scale node: " << r.get_node_ref<Normal>("log_scale")->string() << "\n";
    std::cout << "Data node: " << r.get_node_ref<Normal>("data")->string() << "\n";
    std::cout << "Joint logprob: " << r.get_logprob() << "\n";
    std::cout << "Loglikelihood: " << r.get_loglikelihood() << "\n\n";

    // test function pointer interface (instead of std::function)
    std::cout << "Using function pointer interface.\n";
    std::pair<double, double> fp_result = likelihood_weighting_step(r, normal_model, data);

    std::cout << "Loc node: " << r.get_node_ref<Normal>("loc")->string() << "\n";
    std::cout << "Log scale node: " << r.get_node_ref<Normal>("log_scale")->string() << "\n";
    std::cout << "Data node: " << r.get_node_ref<Normal>("data")->string() << "\n";
    std::cout << "Joint logprob: " << r.get_logprob() << "\n";
    std::cout << "Loglikelihood: " << r.get_loglikelihood() << "\n\n";

    return 0;
}

int test_likelihood_weighting_normal_model() {
    std::cout << "\n~~~ Testing normal model with likelihood weighting ~~~ \n";
    std::function<double(Record<std::string>&,double)> f = normal_model;
    unsigned num_iterations = 4;
    double data = 3.0;
    auto results = likelihood_weighting<std::string, double, double>(f, data, num_iterations);
    Record<std::string> r;

    for (unsigned i = 0; i != num_iterations; i++) {
        r = results.first[i];
        std::cout << "On iteration " << (i + 1) << " of " << num_iterations << "\n";
        std::cout << "Loc node: " << r.get_node_ref<Normal>("loc")->string() << "\n";
        std::cout << "Log scale node: " << r.get_node_ref<Normal>("log_scale")->string() << "\n";
        std::cout << "Data node: " << r.get_node_ref<Normal>("data")->string() << "\n";
        std::cout << "Joint logprob: " << r.get_logprob() << "\n";
        std::cout << "Loglikelihood: " << r.get_loglikelihood() << "\n\n";
    }

    std::cout << "\n~~~ Testing normal model with likelihood weighting, function pointer interface ~~~ \n";
    results = likelihood_weighting<std::string, double, double>(normal_model, data, num_iterations);

    for (unsigned i = 0; i != num_iterations; i++) {
        r = results.first[i];
        std::cout << "On iteration " << (i + 1) << " of " << num_iterations << "\n";
        std::cout << "Loc node: " << r.get_node_ref<Normal>("loc")->string() << "\n";
        std::cout << "Log scale node: " << r.get_node_ref<Normal>("log_scale")->string() << "\n";
        std::cout << "Data node: " << r.get_node_ref<Normal>("data")->string() << "\n";
        std::cout << "Joint logprob: " << r.get_logprob() << "\n";
        std::cout << "Loglikelihood: " << r.get_loglikelihood() << "\n\n";
    }

    return 0;
}

int test_likelihood_weighting_query() {
    std::cout << "\n~~~ Querying normal model after LW inference ~~~ \n";
    unsigned num_iterations = 2500;
    double data = 3.0;
    auto results = likelihood_weighting<std::string, double, double>(normal_model, data, num_iterations);
    auto ir = WeightedSamplePosterior<std::string>(results, true);
    auto qr = ir.query<double>("loc");
    
    for (unsigned ix = 0; ix != num_iterations; ix++) {
        if (ix < 10) {
            std::cout << "Loc #" << ix + 1 << " = " << qr[ix].first << ", weight = " << qr[ix].second << ".\n";
        }
    }

    double computed_mean = ir.reduce<double>("loc", weighted_mean);
    double computed_stddev = ir.reduce<double>("loc", weighted_stddev);
    std::cout << "Prior loc distribution was " << results.first[0].get_node_ref<Normal>("loc")->get_dist().string() << "\n";
    std::cout << "Mean posterior loc after observing data = " << data << " is " << computed_mean << "\n";
    std::cout << "Stddev posterior loc after observing data = " << data << " is " << computed_stddev << "\n";
    return 0;
}

// see test_forward.cpp for description of the following
struct OAD {double rate_guess; double position;};
typedef struct OAD OAD;

unsigned object_assignment(Record<std::string>& r, OAD data) {
    auto num_objects = std::pair<unsigned, double>(0, 0.0);
    while (num_objects.first < 1) {
        num_objects = sample<std::string, std::minstd_rand, unsigned>(r, "num_objects", rng, Poisson(data.rate_guess));
    }
    std::pair<double, double> nth_object_location;
    auto inverse_square_distances = std::vector<double>();
    for (unsigned ix = 0; ix != num_objects.first; ix++) {
        nth_object_location = sample<std::string, std::minstd_rand, double>(r, "object " + std::to_string(ix) + " location", rng, Normal(ix));
        inverse_square_distances.push_back(std::pow(nth_object_location.first - data.position, -2.0));
    }
    normalize_values_ref(inverse_square_distances);
    auto the_object = sample<std::string, std::minstd_rand, unsigned>(r, "object", rng, Categorical(inverse_square_distances));
    auto actual_position = observe<std::string, double>(r, "actual position", data.position, Normal(the_object.first, 0.5));
    return the_object.first;
}

int test_ou_lw() {
    std::cout << "~~~ Testing importance sampling methods and posterior with open universe model ~~~\n";
    OAD data = {.rate_guess = 2.0, .position = 3.15};
    unsigned num_iterations = 250;
    auto results = likelihood_weighting<std::string, OAD, unsigned>(object_assignment, data, num_iterations);
    auto posterior = WeightedSamplePosterior<std::string>(results, true);
    // In contrast to the test_forward.cpp example, this *is* the most likely object...
    unsigned most_likely_object = posterior.reduce<unsigned>("object", mode);
    std::cout << "Most likely object: " << most_likely_object << "\n";
    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_lw_step(),
        test_likelihood_weighting_normal_model(),
        test_likelihood_weighting_query(),
        test_ou_lw()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}
