/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <utility.hpp>
#include <distributions.hpp>
#include <node.hpp>
#include <record.hpp>
#include <forward.hpp>
#include <query.hpp>

#include <cmath>  // for checking rejection sampling worked correctly in program
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <utility>

using namespace Distributions;
using namespace Query;

std::minstd_rand rng(2022);

// probabilistic programs take in a record and a single other input argument of type I
// so we need to wrap multiple input arguments in a struct (or refer to them somehow)
struct OAD {double rate_guess; double position;};
typedef struct OAD OAD;

// the simplest form of open-universe structure: structure is unknown at compile time, but is known as soon as the first random
// variable is drawn
unsigned object_assignment(Record<std::string>& r, OAD data) {
    // How many objects are there?
    // Could implement a truncated poisson, but could also just use rejection sampling...
    auto num_objects = std::pair<unsigned, double>(0, 0.0);
    while (num_objects.first < 1) {
        // Note different behavior from many PPL -- sampling to same address *overwrites without warning*
        // also note that this means the log probability is *wrong* because it's not corrected for change in support
        // to fix this manually we'd need to subtract log probability e^(-rate) from this address's node, which we won't do here.
        num_objects = sample<std::string, std::minstd_rand, unsigned>(r, "num_objects", rng, Poisson(data.rate_guess));
    }
    std::pair<double, double> nth_object_location;
    auto inverse_square_distances = std::vector<double>();

    for (unsigned ix = 0; ix != num_objects.first; ix++) {
        // The objects are lined up in a row, though not evenly
        nth_object_location = sample<std::string, std::minstd_rand, double>(r, "object " + std::to_string(ix) + " location", rng, Normal(ix));
        // which object emitted the particle we observed? likelihood is propotional to inverse square of the distance from the object
        inverse_square_distances.push_back(std::pow(nth_object_location.first - data.position, -2.0));
    }
    normalize_values_ref(inverse_square_distances);
    // actually assign the object
    auto the_object = sample<std::string, std::minstd_rand, unsigned>(r, "object", rng, Categorical(inverse_square_distances));
    return the_object.first;
}

int test_ou_run() {
    auto record = Record<std::string>();
    OAD data = {.rate_guess = 2.0, .position = 3.15};
    object_assignment(record, data);
    unsigned nobj = *record.get_value_ref<unsigned>("num_objects");
    std::cout << "Hypothesized number of objects: " << nobj << "\n";
    std::cout << "Node corresponding to this number of objects: " << record.get_node_ref<Poisson>("num_objects")->string() << "\n";
    // NOTE: per docurmentation in object_assignment this is actually off by a factor of e^(-rate)! But we want to check it aligns
    // which is why we aren't correcting it
    double correct_logprob = -data.rate_guess + nobj * std::log(data.rate_guess) - lgamma(nobj + 1.0);
    std::cout << "Correct log probability is: " << correct_logprob << "\n";
    if (!double_allclose(correct_logprob, record.get_node_ref<Poisson>("num_objects")->get_logprob())) {
        std::cout << "Node logprob doesn't match analytical logprob!\n";
        return 1;
    }
    return 0;
}

int test_ou_forward() {
    std::cout << "~~~ Testing forward sampling methods and posterior ~~~\n";
    OAD data = {.rate_guess = 2.0, .position = 3.15};
    unsigned num_iterations = 250;
    auto results = forward<std::string, OAD, unsigned>(object_assignment, data, num_iterations);
    std::cout << "First sampled object: " << *results.first[0].template get_value_ref<unsigned>("object") << "\n";
    std::cout << "First sampled node: " << results.first[0].template get_node_ref<Categorical>("object")->string() << "\n";
    auto posterior = WeightedSamplePosterior<std::string>(results, true);
    // This is *not* the most likely object -- just testing that the mode reducer works correctly.
    unsigned most_likely_object = posterior.reduce<unsigned>("object", mode);
    std::cout << "Most likely object: " << most_likely_object << "\n";
    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_ou_run(),
        test_ou_forward()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}
