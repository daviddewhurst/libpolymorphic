/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <distributions.hpp>
#include <node.hpp>
#include <record.hpp>
#include <interpretations.hpp>

#include <iostream>
#include <string>
#include <random>
#include <utility>

using namespace Distributions;

std::minstd_rand rng(2022);

int test_replay_lowlevel() {
    std::cout << "~~~ Low-level replay test ~~~\n";
    auto r = Record<char>();
    auto original = sample<char, std::minstd_rand, double, Normal>(r, 'a', rng, Normal());
    std::cout << "Sampled " << original.first << " with node " << r.template get_node_ref<Normal>('a')->string() << "\n";
    std::cout << "Note value in record is also " << *r.template get_value_ref<double>('a') << "\n";
    std::cout << "Total record logprob: " << r.get_logprob() << "\n";
    
    // now change interpretations and set a different value in the record
    // at a higher level we might call this as a function, e.g.,
    // replay(r, 'a', 10.0);
    r.template get_node_ref<Normal>('a')->set_interpretation(REPLAY);
    r.template set_value<double>('a', 10.0);
    
    // note exact same function call!
    // this allows diverse interpretations of a single stochstic program
    auto changed = sample<char, std::minstd_rand, double, Normal>(r, 'a', rng, Normal());
    std::cout << "Replayed " << changed.first << " into record, resulting node " << r.template get_node_ref<Normal>('a')->string() << "\n";
    std::cout << "Note value in record is also " << *r.template get_value_ref<double>('a') << "\n";
    std::cout << "Total record logprob: " << r.get_logprob() << "\n";
    return 0;
}

int test_replay() {
    std::cout << "~~~ Replay test ~~~\n";
    auto r = Record<char>();
    auto original = sample<char, std::minstd_rand, double, Normal>(r, 'a', rng, Normal());
    std::cout << "Sampled " << original.first << " with node " << r.template get_node_ref<Normal>('a')->string() << "\n";
    std::cout << "Note value in record is also " << *r.template get_value_ref<double>('a') << "\n";
    std::cout << "Total record logprob: " << r.get_logprob() << "\n";
    
    // implementation of hypothesized syntax above
    replay<char, double, Normal>(r, 'a', 10.0);
    
    // note exact same function call!
    // this allows diverse interpretations of a single stochstic program
    auto changed = sample<char, std::minstd_rand, double, Normal>(r, 'a', rng, Normal());
    std::cout << "Replayed " << changed.first << " into record, resulting node " << r.template get_node_ref<Normal>('a')->string() << "\n";
    std::cout << "Note value in record is also " << *r.template get_value_ref<double>('a') << "\n";
    std::cout << "Total record logprob: " << r.get_logprob() << "\n";

    return 0;
}

double normal_model(Record<char>& r, double data) {
    auto loc = sample<char, std::minstd_rand, double>(r, 'a', rng, Normal());
    auto scale = sample<char, std::minstd_rand, double>(r, 'b', rng, Gamma());
    auto data_node = observe<char, double>(r, 'c', data, Normal(loc.first, scale.first));
    return data_node.first;
}

int test_integration() {
    std::cout << "~~~ Integration test of interpretations ~~~\n";
    auto r = Record<char>();
    double data = -2.0;

    // first, get a sample from the program
    double retval = normal_model(r, data);
    std::cout << r.template summarize<double, Normal>('a') << "\n";
    std::cout << r.template summarize<double, Gamma>('b') << "\n";
    std::cout << r.template summarize<double, Normal>('c') << "\n";

    std::cout << "\nReplaying...\n";
    // now, replay a different value against the program
    // let's set the variance to something else
    replay<char, double, Gamma>(r, 'b', 4.0);
    retval = normal_model(r, data);
    std::cout << r.template summarize<double, Normal>('a') << "\n";
    std::cout << r.template summarize<double, Gamma>('b') << "\n";
    std::cout << r.template summarize<double, Normal>('c') << "\n";

    // stop replaying, switch back to standard interpretation and draw a different sample
    std::cout << "\nGoing back to standard...\n";
    standard<char, Gamma>(r, 'b');
    retval = normal_model(r, data);
    std::cout << r.template summarize<double, Normal>('a') << "\n";
    std::cout << r.template summarize<double, Gamma>('b') << "\n";
    std::cout << r.template summarize<double, Normal>('c') << "\n";

    // actually, we want to switch the scale distribution! instead of adding a
    // hyperprior, we can just change out our priors "manually"
    std::cout << "\nSetting different prior...\n";
    redistribute<char, double, Gamma, Gamma>(r, 'b', Gamma(3.0, 2.0));
    retval = normal_model(r, data);
    std::cout << r.template summarize<double, Normal>('a') << "\n";
    std::cout << r.template summarize<double, Gamma>('b') << "\n";
    std::cout << r.template summarize<double, Normal>('c') << "\n";

    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_replay_lowlevel(),
        test_replay(),
        test_integration()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}
