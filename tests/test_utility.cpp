#include <utility.hpp>

#include <cmath>
#include <iostream>
#include <vector>


int test_sumexp_basic() {

    auto zeros = std::vector<double>({0.0, 0.0, 0.0});
    double sumexp_zeros, other_sumexp_zeros;
    sumexp_zeros = sumexp(zeros, false);
    std::cout << "sumexp of three zeros: " << sumexp_zeros << "\n";

    other_sumexp_zeros = sumexp(zeros);  // safe = true by default
    std::cout << "safe(r) sumexp of three zeros: " << other_sumexp_zeros << "\n";
    
    if (!double_allclose(sumexp_zeros, other_sumexp_zeros)) {
        std::cout << "unsafe sumexp = " << sumexp_zeros << " while safe sumexp = " << other_sumexp_zeros << "!\n";
        return 1;
    }

    return 0;
}

int test_logsumexp_basic() {
    auto zeros = std::vector<double>({0.0, 0.0, 0.0});
    double lse, other_lse;
    lse = logsumexp(zeros);
    other_lse = std::log(3.0);
    std::cout << "Computed lse = " << lse << " and analytic result is " << other_lse << "\n";
    if (!double_allclose(lse, other_lse)) {
        std::cout << "computed lse = " << lse << " while analytic result is = " << other_lse << "!\n";
        return 1;
    }

    return 0;
}


int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_sumexp_basic(),
        test_logsumexp_basic()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}
