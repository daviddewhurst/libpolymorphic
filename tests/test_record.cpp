/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <distributions.hpp>
#include <node.hpp>
#include <record.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <utility>

using namespace Distributions;

std::minstd_rand rng(2022);

int test_make_record() {

    auto r = Record<std::string>();

    // this isn't the interface we want, but something like this will happen underneath
    auto address = "loc";
    auto value_and_node = sample<std::minstd_rand, double, Normal>(rng, Normal());
    r.set_node(address, value_and_node.second);
    r.set_value(address, value_and_node.first);

    Node<Normal> * n_ref = r.get_node_ref<Normal>(address);
    std::cout << "Accessed underlying node at address " << address << ": " << n_ref->string() << "\n";
    // n_ref->set_value(10.0); // n_ref.set_value(10.0);
    n_ref->set_logprob(10.0);
    Node<Normal> * other_n_ref = r.get_node_ref<Normal>(address);
    std::cout << "Accessed underlying node at address " << address << ": " << other_n_ref->string() << "\n";
    return 0;
}

void verbose_normal_model(Record<std::string>& r) {
    auto loc = sample<std::minstd_rand, double, Normal>(rng, Normal());
    r.set_node("loc", std::move(loc.second));
    auto log_scale = sample<std::minstd_rand, double, Normal>(rng, Normal(0.0, 2.0));
    r.set_node("log_scale", std::move(log_scale.second));
    auto data = sample<std::minstd_rand, double, Normal>(rng, Normal(loc.first, std::exp(log_scale.first)));
    r.set_node("data", std::move(data.second));
}

int test_verbose_normal_model() {
    std::cout << "\n~~~ Testing verbose normal model ~~~ \n";
    auto r = Record<std::string>();
    verbose_normal_model(r);
    std::cout << "Loc node: " << r.get_node_ref<Normal>("loc")->string() << "\n";
    std::cout << "Log scale node: " << r.get_node_ref<Normal>("log_scale")->string() << "\n";
    std::cout << "Data node: " << r.get_node_ref<Normal>("data")->string() << "\n";
    std::cout << "Joint logprob: " << r.get_logprob() << "\n\n";

    // note that we can re-use the same record!
    verbose_normal_model(r);
    std::cout << "Loc node: " << r.get_node_ref<Normal>("loc")->string() << "\n";
    std::cout << "Log scale node: " << r.get_node_ref<Normal>("log_scale")->string() << "\n";
    std::cout << "Data node: " << r.get_node_ref<Normal>("data")->string() << "\n";
    std::cout << "Joint logprob: " << r.get_logprob() << "\n";
    std::cout << "Loglikelihood: " << r.get_loglikelihood() << "\n\n";

    return 0;
}

double normal_model(Record<std::string>& r, double data) {
    auto loc = sample<std::string, std::minstd_rand, double, Normal>(r, "loc", rng, Normal()); 
    auto log_scale = sample<std::string, std::minstd_rand, double, Normal>(r, "log_scale", rng, Normal(0.0, 3.0));
    auto data_ = observe<std::string, double, Normal>(r, "data", data, Normal(loc.first, std::exp(log_scale.first)));
    return std::pow(data_.first, 2.0);
}

int test_normal_model() {
    std::cout << "\n~~~ Testing normal model ~~~ \n";
    auto r = Record<std::string>();
    double data = 3.0;
    auto data_sq = normal_model(r, data);
    std::cout << "Loc node: " << r.get_node_ref<Normal>("loc")->string() << "\n";
    std::cout << "Log scale node: " << r.get_node_ref<Normal>("log_scale")->string() << "\n";
    std::cout << "Data node: " << r.get_node_ref<Normal>("data")->string() << "\n";
    std::cout << "Joint logprob: " << r.get_logprob() << "\n";
    std::cout << "Loglikelihood: " << r.get_loglikelihood() << "\n\n";
    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_make_record(),
        test_verbose_normal_model(),
        test_normal_model()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}