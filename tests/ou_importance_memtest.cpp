/* 
 * This file is part of libpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <distributions.hpp>
// #include <node.hpp>
#include <record.hpp>
#include <query.hpp>
#include <importance.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <utility>

using namespace Distributions;
using namespace Query;

std::minstd_rand rng(2022);

// see test_forward.cpp for description of the following
struct OAD {double rate_guess; double position;};
typedef struct OAD OAD;

unsigned object_assignment(Record<int>& r, OAD data) {
    int address_num = 0;
    auto num_objects = std::pair<int, double>(0, 0.0);
    while (num_objects.first < 1) {
        num_objects = sample<int, std::minstd_rand, unsigned>(r, address_num, rng, Poisson(data.rate_guess));
    }
    std::pair<double, double> nth_object_location;
    auto inverse_square_distances = std::vector<double>();
    for (unsigned ix = 0; ix != num_objects.first; ix++) {
        ++address_num;
        nth_object_location = sample<int, std::minstd_rand, double>(r, address_num, rng, Normal(ix));
        inverse_square_distances.push_back(std::pow(nth_object_location.first - data.position, -2.0));
    }
    normalize_values_ref(inverse_square_distances);
    auto the_object = sample<int, std::minstd_rand, unsigned>(r, -1, rng, Categorical(inverse_square_distances));
    ++address_num;
    auto actual_position = observe<int, double>(r, address_num, data.position, Normal(the_object.first, 0.5));
    return the_object.first;
}

int test_ou_lw() {
    std::cout << "~~~ Testing importance sampling methods and posterior with open universe model ~~~\n";
    OAD data = {.rate_guess = 2.0, .position = 3.15};
    unsigned num_iterations = 250;
    auto results = likelihood_weighting<int, OAD, unsigned>(object_assignment, data, num_iterations);
    auto posterior = WeightedSamplePosterior<int>(results, true);
    // In contrast to the test_forward.cpp example, this *is* the most likely object...
    // denote query address by -1 instead of all other addresses, which are rep-d as positive integers
    unsigned most_likely_object = posterior.reduce<unsigned>(1, mode);
    std::cout << "Most likely object: " << most_likely_object << "\n";
    return 0;
}

int test_ou_lw_stream() {
    std::cout << "~~~ Testing importance sampling methods and posterior with open universe model ~~~\n";
    OAD data = {.rate_guess = 2.0, .position = 3.15};
    unsigned num_iterations = 2500000;
    auto stat = StreamMode<unsigned, int>();
    unsigned most_likely_object = likelihood_weighting<int, OAD, unsigned>(
        object_assignment, data, stat, -1, num_iterations
    );
    std::cout << "Most likely object: " << most_likely_object << "\n";

    return 0;
}

// compiled with apple clang:
// RAM usage as measured with /usr/bin/time -v ./mem_test, maximum resident set size
// roughly 1.3MB max RAM usage, still obtains correct posterior mode -- using std::string addresses
// roughly 1.24MB max RAM usage -- using int addresses
// roughly 905KB max RAM usage -- using streaming computation, int addresses

// compiled with g++ on manjaro
// RAM usage as measured with ps_mem -S -p $(psgrep mem_test)
// using streaming computation, int addresses
// [drd@david-labtop ~]$ ps_mem -S -p $(pgrep mem_test)
//  Private  +   Shared  =  RAM used   Swap used	Program

// 292.0 KiB + 612.0 KiB = 904.0 KiB     0.0 KiB	mem_test
// ---------------------------------------------
//                         904.0 KiB     0.0 KiB
// =============================================
// note that the bug in gnu time leads to erroneous result of 4x this value 
// because of assumption of measurements in page size instead of KB



int main(int argc, char ** argv) {
        //test_ou_lw();
        test_ou_lw_stream();
}
