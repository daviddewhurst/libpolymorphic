#!/usr/bin/env bash

./utility_test || exit 1
./dist_test || exit 1
./node_test || exit 1
./record_test || exit 1
./importance_test || exit 1
./forward_test || exit 1